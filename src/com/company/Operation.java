package com.company;

public enum Operation {
	ADD {
	},
	SUB {
	},
	MUL {
	},
	DIV {
	};

	double get(double a, double b) {

		switch (this) {
			case ADD:
				return a + b;
			case SUB:
				return a - b;
			case MUL:
				return a * b;
			case DIV:
				return a / b;
			default:
				break;
		}
		return 0;
	}
}
